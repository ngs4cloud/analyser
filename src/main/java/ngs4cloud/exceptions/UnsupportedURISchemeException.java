package ngs4cloud.exceptions;

public class UnsupportedURISchemeException extends RuntimeException {
  public UnsupportedURISchemeException(String scheme) {
    super("NGS4Cloud only supports http, https, file, ftp schemes. " + scheme + " is not supported.");
  }
}
