package ngs4cloud;

import ngs4cloud.dslprocessor.PipelineFactory;
import ngs4cloud.intermediate_representation.*;
import parser.PipelineParser;
import repository.entities.Input;
import repository.support.managers.Support;
import repository.entities.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class PipeCtxToIRMapper {

  private final static String LSTDIRS = "ngs4clouddir";
  private int nFiles = -1;
  private LinkedList<String> referencesList = new LinkedList<>();

  private HashMap<Integer, PairTC> pairs = new HashMap<>();
  private HashMap<Integer, List<Input>> inputs = new HashMap<>();
  HashMap<Integer, List<Output>> outputs = new HashMap<>();
  private boolean isListing = false;
  private IntermediateRepresentation ir;

  public IntermediateRepresentation map(PipelineParser.PipelineContext pipelineCtx) {
    Pipeline pipeline = new PipelineFactory().make(pipelineCtx);
    return createIRFromPipeline(pipeline);
  }

  private IntermediateRepresentation createIRFromPipeline(Pipeline pipeline) {
    ir = new IntermediateRepresentation();
    List<Command> commands = pipeline.tools.stream().flatMap(tool -> tool.commands.stream()).collect(Collectors.toList());
    for (Command command : commands) {
      if (command.name.equals("startListing")) {

        if (isListing)
          throw new IllegalStateException("Can't start a listing when you haven't stopped the current one.");
        isListing = true;
        ArrayList<String> filesList = getListOfFileNames("filesList", command);
        filesList.forEach(fileName ->
            command.inputs.add(new Input("", "", "", "", fileName, command))
        );
        nFiles = filesList.size();
        fixListingOutputs(command);
        addTask(command);

      } else if (command.name.equals("restartListing")) {
        if (isListing)
          throw new IllegalStateException("Can't restart a listing when you haven't stopped the current one.");
        isListing = true;

      } else if (command.name.equals("stopListing")) {
        isListing = false;
        ArrayList<String> filesList = getListOfFileNames("destinationFiles", command);
        String refName = command.arguments.stream().filter((arg) -> arg.name.equals("referenceName")).findFirst().get().value;
        for (int k = 1; k <= filesList.size(); ++k) {
          command.inputs.add(new Input("", "", "", "", LSTDIRS + k + "/" + refName, command));
        }
        command.outputs.add(new Output("", "", "", "", refName, command));
        addTask(command);

      } else {
        if (isListing) {
          LinkedList<String> appliedRefs = referencesList.stream().filter(ref -> command.inputs.stream().filter(ipt -> ipt.value.equals(ref)).findFirst().isPresent()).collect(Collectors.toCollection(LinkedList::new));
          referencesList.addAll(command.outputs.stream().map(output -> output.value).collect(Collectors.toList()));
          new Combinator(nFiles, appliedRefs, command).combine();
        } else {
          addTask(command);
        }
      }
    }
    addParents();
    addDirectories();
    ir.outputs = outputs.values().stream().flatMap(Collection::stream).map(opt -> opt.value).collect(Collectors.toCollection(LinkedList::new));
    return ir;
  }

  private ArrayList<String> getListOfFileNames(String argumentName, Command command) {
    String filesListString = command.arguments.stream().filter((arg) -> arg.name.equals(argumentName)).findFirst().get().value;
    return new ArrayList<String>(Arrays.asList(filesListString.split(" ")));
  }

  private void fixListingOutputs(Command command) {
    Output oldOutput = command.outputs.get(0);
    referencesList.add(oldOutput.value);
    command.outputs = new LinkedList<>();
    for (int i = 1; i <= nFiles; ++i) {
      command.outputs.add(new Output(oldOutput.name, oldOutput.description, oldOutput.outputType, oldOutput.argumentName, LSTDIRS + i + "/" + oldOutput.value, command));
    }
  }

  private void addTask(Command command) {
    Task task = new Task(command.tool.dockerImage, command.command + " " + Support.getComposer(command.argumentsComposer)
        .compose(command.arguments), command.tool.requiredMemory, command.tool.recommendedDiskSpace, command.tool.recommendedCpus);
    pairs.put(task.id, new PairTC(command.tool, command));
    inputs.put(task.id, command.inputs);
    outputs.put(task.id, command.outputs);
    ir.tasks.add(task);
  }

  private void addDirectories() {
    inputs.values().stream().flatMap(c -> c.stream()).forEach(input -> {
      if (!input.value.matches("")) {
        Path inputPath = Paths.get(input.value);
        Path parentPath = inputPath.getParent();
        if (parentPath != null) {
          String parent = parentPath.toString();
          ir.directories.add(parent);
        }
      }
    });
    outputs.values().stream().flatMap(c -> c.stream()).forEach(output -> {
      if (!output.value.matches("")) {
        Path outputPath = Paths.get(output.value);
        Path parentPath = outputPath.getParent();
        if (parentPath != null) {
          String parent = parentPath.toString();
          ir.directories.add(parent);
        }
      }
    });
    pairs.values().stream().map(p -> p.command).forEach(command -> command.arguments.stream().forEach(arg -> {
      if (arg.getArgumentType().matches("directory")) {
        ir.directories.add(arg.value);
      }
    }));
    ir.directories = ir.directories.stream().distinct().collect(Collectors.toList());
  }

  private void addParents() {
    for (Task task : ir.tasks) {
      for (Task otherTask : ir.tasks) {
        if (task == otherTask) break;
        if (inputs.get(task.id).stream()
            .anyMatch(ipt -> outputs.get(otherTask.id).stream().anyMatch(opt -> ipt.value.equals(opt.value) && !ipt.value.equals("")))) {
          task.parents.add(otherTask.id);
        }
      }
    }
  }

  private class Combinator {

    private final static String LSTDIRS = "ngs4clouddir";

    private int nFiles;
    private LinkedList<String> refs;
    private Command template;
    private int nRefs;
    private int[] vals;

    public Combinator(int nFiles, LinkedList<String> refs, Command template) {
      this.nFiles = nFiles;
      this.refs = refs;
      this.template = template;
      vals = new int[refs.size()];
      nRefs = refs.size();
    }

    public void combine() {
      int n = 0;
      combine(n);
    }

    private void combine(int n) {
      for (int i = 1; i <= nFiles; ++i) {
        vals[n] = i;
        if (n < nRefs-1) {
          combine(++n);
          --n;
        } else {
          Command newCommand = new Command(template.name, template.command, template.description, template.priority, template.argumentsComposer, template.tool);
          newCommand.inputs = new LinkedList<>();
          newCommand.inputs.addAll(template.inputs.stream().filter(ipt -> {
            for (String ref :
                refs) {
              if (ipt.value.equals(ref)) {
                return false;
              }
            }
            return true;
          }).map(input -> new Input(input.name, input.description, input.inputType, input.argumentName, input.value, newCommand)).collect(Collectors.toList()));
          int j = 0;
          for (String ref : refs) {
            Input oldInput = template.inputs.stream().filter(ipt -> ipt.value.equals(ref)).findFirst().get();
            newCommand.inputs.add(new Input(oldInput.name, oldInput.description, oldInput.inputType, oldInput.argumentName, LSTDIRS + vals[j] + "/" + oldInput.value, newCommand));
            ++j;
          }
          for (Output output : template.outputs) {
            newCommand.outputs = new LinkedList<>();
            String prefix = "";
            for (int val : vals) {
              prefix += LSTDIRS + val + "/";
            }
            newCommand.outputs.add(new Output(output.name, output.description, output.outputType, output.argumentName, prefix + output.value, newCommand));
          }
          fixCommandArguments(template, newCommand);
          addTask(ir, newCommand);
        }
      }
    }

    private void fixCommandArguments(Command command, Command newCommand) {
      newCommand.arguments = new LinkedList<>();
      command.arguments.forEach(arg -> {
        Argument newArg;
        newArg = new Argument(arg.name, arg.argumentType, arg.isRequired, arg.getDescription(), arg.value, newCommand);
        newCommand.arguments.add(newArg);
      });
      for (Input ipt : newCommand.inputs) {
        newCommand.arguments.stream().filter(arg -> arg.name.equals(ipt.argumentName)).forEach(arg -> {
          arg.value = ipt.value;
        });
      }
      for (Output opt : newCommand.outputs) {
        newCommand.arguments.stream().filter(arg -> arg.name.equals(opt.argumentName)).forEach(arg -> arg.value = opt.value);
      }
    }

    private void addTask(IntermediateRepresentation ir, Command command) {
      Task task = new Task(command.tool.dockerImage, command.command + " " + Support.getComposer(command.argumentsComposer)
          .compose(command.arguments), command.tool.requiredMemory, command.tool.recommendedDiskSpace, command.tool.recommendedCpus);
      pairs.put(task.id, new PipeCtxToIRMapper.PairTC(command.tool, command));
      inputs.put(task.id, command.inputs);
      outputs.put(task.id, command.outputs);
      ir.tasks.add(task);
    }

  }


  private class PairTC {
    public Tool tool;
    public Command command;

    public PairTC(Tool t, Command c) {
      tool = t;
      command = c;
    }
  }

}
