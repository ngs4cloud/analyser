package ngs4cloud.intermediate_representation;

import java.util.LinkedList;
import java.util.List;

public class IntermediateRepresentation {
    public String input;
    public List<Task> tasks = new LinkedList<>();
    public List<String> outputs;
    public List<String> directories = new LinkedList<>();
}
