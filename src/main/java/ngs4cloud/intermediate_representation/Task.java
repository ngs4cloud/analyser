package ngs4cloud.intermediate_representation;

import java.util.LinkedList;
import java.util.List;

public class Task {

    static int curId;

    public int id;
    public String dockerImage;
    public String command;
    public int mem;
    public int disk;
    public int cpus;
    public List<Integer> parents;

    public Task(String dockerImage, String command, int mem, int disk, int cpus){
        this.id = ++curId;
        this.dockerImage = dockerImage;
        this.command = command;
        this.mem = mem;
        this.disk = disk;
        this.cpus = cpus;
        this.parents = new LinkedList<>();
    }

}
