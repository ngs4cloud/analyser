package ngs4cloud;

import com.google.gson.*;
import ngs4cloud.exceptions.NoSuchOutputWillBeGeneratedException;
import ngs4cloud.exceptions.UnsupportedURISchemeException;
import ngs4cloud.intermediate_representation.IntermediateRepresentation;
import parser.PipelineParser;
import parser.PipesJavaParser;

import java.io.*;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Analyser {

  boolean allOutputs = false;

  public void analyse(String pipesFilePath, String destFilePath, String inputFilePath, List<String> outputs) throws IOException {
    try {
      validateInputFilePath(inputFilePath);
      PipelineParser.PipelineContext pipelineCtx = new PipesJavaParser().parsePipes(pipesFilePath);
      PipeCtxToIRMapper pipeCtxToIRMapper = new PipeCtxToIRMapper();
      IntermediateRepresentation ir = pipeCtxToIRMapper.map(pipelineCtx);
      if (!allOutputs) {
        validateOutputsExist(pipeCtxToIRMapper, outputs);
      }
      ir.input = inputFilePath;
      if (outputs != null) {
        ir.outputs = outputs;
      }
      fixOutputs(ir.outputs);
      ir.directories = fixDirectories(ir.directories);
      writeIRJsonToFile(ir, destFilePath);
    } catch (Exception e) {
      //e.printStackTrace();
      String message = e.getMessage();
      if (!message.isEmpty()) {
        System.out.println(message);
      }
    }
  }

  private LinkedList<String> fixDirectories(List<String> directories) {
    LinkedList<String> newDirs = new LinkedList();
    for (String dir : directories) {
      newDirs.add(dir.replace("\\", "/"));
    }
    return newDirs;
  }

  private void validateOutputsExist(PipeCtxToIRMapper pipeCtxToIRMapper, List<String> outputs) {
    for (String output :
        outputs) {
      //pipeCtxToIRMapper.outputs.values().stream().flatMap(List::stream).collect(Collectors.toList()).stream().forEach((opt) -> System.out.println(opt.name + " = " + opt.value));
      pipeCtxToIRMapper.outputs.values().stream().flatMap(List::stream).collect(Collectors.toList())
          .stream().filter(opt -> opt.value.equals(output)).findFirst()
          .orElseThrow(() -> new NoSuchOutputWillBeGeneratedException(output));
    }
  }

  private void fixOutputs(List<String> outputs) {
    for (String output : outputs) {
      output = output.replace("\\", "/");
    }
  }

  private void validateInputFilePath(String inputFilePath) {
    try {
      URI uri = URI.create(inputFilePath);
      if (!uri.getScheme().equalsIgnoreCase("http") && !uri.getScheme().equalsIgnoreCase("file") && !uri.getScheme().equalsIgnoreCase("https") && !uri.getScheme().equalsIgnoreCase("ftp")) {
        throw new UnsupportedURISchemeException(uri.getScheme());
      }
    } catch (Exception e) {
      if (e instanceof UnsupportedURISchemeException) throw e;
      throw e;
    }
  }

  private void writeIRJsonToFile(IntermediateRepresentation ir, String destFilePath) throws IOException {
    try (Writer jsonWriter = new FileWriter(destFilePath)) {
      Gson gson = new GsonBuilder().disableHtmlEscaping().create();
      gson.toJson(ir, jsonWriter);
    }
  }

  public static void main(String[] args) throws IOException {
    Analyser analyser = new Analyser();
    if (args.length == 0) {
      System.out.println("Please insert a command.");
      System.out.println("Available commands: \n" +
          "analyse -pipes $description_filepath -ir $ir_destiny_filepath -input $input_URI -outputs $space_separated_list_of_outputs");
    } else if (!args[0].equalsIgnoreCase("analyse")
        || args.length < 7
        || !args[1].equals("-pipes")
        || !args[3].equals("-ir")
        || !args[5].equals("-input")) {

      System.out.println("Invalid command: " + Arrays.stream(args).collect(Collectors.joining(" ")));
      System.out.println("Available commands: \n" +
          "analyse -pipes $description_filepath -ir $ir_destiny_filepath -input $input_URI [ -outputs $space_separated_list_of_outputs ]");
    } else {
      if (args.length < 8) {
        analyser.allOutputs = true;
        analyser.analyse(args[2], args[4], args[6], null);
      } else {
        analyser.analyse(args[2], args[4], args[6], Arrays.asList(args).subList(8, args.length));
      }
    }
    System.out.println("Analyser finished execution.");
  }

}
