package ngs4cloud.dslprocessor;

import ngs4cloud.dslprocessor.exceptions.argumentstype.IncorrectArgumentTypeException;
import ngs4cloud.dslprocessor.exceptions.argumentstype.NoSuchArgumentTypeException;
import ngs4cloud.dslprocessor.exceptions.argumentstype.FlagArgumentException;
import ngs4cloud.dslprocessor.exceptions.chain.*;
import parser.PipelineParser;
import repository.RepositoryFactory;
import repository.descriptors.IArgumentDescriptor;
import repository.descriptors.ICommandDescriptor;
import repository.descriptors.IToolDescriptor;
import repository.repository.IRepository;
import repository.entities.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class PipelineFactory {

  private Pipeline pipeline;
  private IRepository repository;
  private Command lastCommand;
  private Tool curTool;

  public Pipeline make(PipelineParser.PipelineContext pipelineCtx) {
    repository = new RepositoryFactory().make(pipelineCtx.repositoryType().getText(),
        pipelineCtx.repositoryLocation().getText());
    Repository lRepo = new Repository(repository.getLocation(),
        repository.getType());
    pipeline = new Pipeline(lRepo);
    addTools(pipelineCtx.tool());
    return pipeline;
  }

  private void addTools(List<PipelineParser.ToolContext> toolsCtx) {
    for (PipelineParser.ToolContext toolCtx : toolsCtx) {
      IToolDescriptor toolDescriptor = repository.getTool(toolCtx.toolName().getText());
      addTool(toolDescriptor, toolCtx);
    }
  }

  private void addTool(IToolDescriptor toolDescriptor, PipelineParser.ToolContext toolCtx) {
    Tool tool = new Tool(toolDescriptor.getName(), toolDescriptor.getAuthor(), toolDescriptor.getVersion(),
        toolDescriptor.getDescription(), toolDescriptor.getDocumentation(),
        toolDescriptor.getRequiredMemory(), toolDescriptor.getRecommendedCpus(),
        toolDescriptor.getRecommendedDiskSpace(),
        repository.getConfigurationFor(toolDescriptor.getName(), toolCtx.configuration().getText()).getUri(),
        toolDescriptor.getType(), pipeline
    );
    curTool = tool;
    addCommands(tool, toolDescriptor, toolCtx.command());
    pipeline.tools.add(tool);
  }

  private void addCommands(Tool tool, IToolDescriptor toolDescriptor, List<PipelineParser.CommandContext> commandsCtx) {
    for (PipelineParser.CommandContext commandCtx : commandsCtx) {
      ICommandDescriptor commandDescriptor = toolDescriptor.getCommand(commandCtx.commandName().getText());
      if (commandDescriptor == null)
        throw new NullPointerException("No such command in " + tool.name +" Descriptor.json " + commandCtx.commandName().getText());
      Command command = new Command(commandDescriptor.getName(), commandDescriptor.getCommand(),
          commandDescriptor.getDescription(), commandDescriptor.getPriority(),
          commandDescriptor.getArgumentsComposer(), tool);
      command.arguments = getArguments(commandDescriptor, commandCtx.argument(), commandCtx.chain(), command);
      command.outputs = getOutputs(commandDescriptor, command);
      command.inputs = getInputs(commandDescriptor, command);
      fixArguments(command);
      matchInputsOutputsAndArguments(command);
      fixInputsAndOuputs(command);
      lastCommand = command;
      tool.commands.add(command);
    }
  }

  private LinkedList<Output> getOutputs(ICommandDescriptor commandDescriptor, Command command) {
    return commandDescriptor.getOutputs().stream().map(
        outputDescriptor -> {
          return new Output(outputDescriptor.getName(), outputDescriptor.getDescription(),
              outputDescriptor.getType(), outputDescriptor.getArgumentName(), outputDescriptor.getValue(), command);
        }
    ).collect(Collectors.toCollection(LinkedList::new));
  }

  private LinkedList<Input> getInputs(ICommandDescriptor commandDescriptor, Command command) {
    return commandDescriptor.getInputs().stream().map(inputDescriptor -> {
      return new Input(
          inputDescriptor.getName(), inputDescriptor.getDescription(),
          inputDescriptor.getType(), inputDescriptor.getArgumentName(), inputDescriptor.getValue(), command);
        }
    ).collect(Collectors.toCollection(LinkedList::new));
  }

  private LinkedList<Argument> getArguments(ICommandDescriptor commandDescriptor, List<PipelineParser.ArgumentContext> argumentsCtx, List<PipelineParser.ChainContext> chainsCtx, Command command) {
    LinkedList<Argument> arguments = new LinkedList<>();
    for (PipelineParser.ArgumentContext argumentCtx : argumentsCtx) {
      IArgumentDescriptor argumentDescriptor = commandDescriptor.getArgument(argumentCtx.argumentName().getText());
      if (argumentDescriptor == null)
        throw new NoSuchArgumentInSpecifiedToolCommandException(argumentCtx.argumentName().getText(), commandDescriptor.getName(), commandDescriptor.getOriginTool().getName());
      throwIfArgumentTypesDoNotMatch(argumentDescriptor.getType(), argumentCtx.argumentValue().getText(), argumentDescriptor);
      arguments.add(new Argument(argumentDescriptor.getName(), argumentDescriptor.getType(), argumentDescriptor.getRequired(), argumentDescriptor.getDescription(), argumentCtx.argumentValue().getText(), command));
    }
    for (PipelineParser.ChainContext chainCtx : chainsCtx) {
      IArgumentDescriptor argumentDescriptor = commandDescriptor.getArgument(chainCtx.argumentName().getText());
      if (argumentDescriptor == null)
        throw new NoSuchArgumentInSpecifiedToolCommandException(chainCtx.argumentName().getText(), commandDescriptor.getName(), commandDescriptor.getOriginTool().getName());
      arguments.add(new Argument(argumentDescriptor.getName(), argumentDescriptor.getType(), argumentDescriptor.getRequired(), argumentDescriptor.getDescription(), getValueFromChain(chainCtx), command));
    }
    return arguments;
  }

  private void throwIfArgumentTypesDoNotMatch(String type, String value, IArgumentDescriptor argumentDescriptor) {
    try {
      switch (type) {
        case "int":
          Integer.parseInt(value);
          break;
        case "double":
          Double.parseDouble(value);
          break;
        case "flag":
          if (!value.equals("NA")) {
            throw new FlagArgumentException(argumentDescriptor, value);
          }
          break;
        case "file":
        case "directory":
        case "string":
          break;
        default:
          throw new NoSuchArgumentTypeException(type);
      }
    } catch (NumberFormatException e) {
      throw new IncorrectArgumentTypeException(argumentDescriptor.getOriginCommand().getOriginTool().getName(),
          argumentDescriptor.getOriginCommand().getName(), argumentDescriptor.getName(), value, type);
    }
  }

  private String getValueFromChain(PipelineParser.ChainContext chainCtx) {
    if (chainCtx.commandName() == null) {
      return getValueFromChainedArgument(lastCommand, () -> new NoSuchOutputInPreviousCommandException(chainCtx.outputName().getText(), lastCommand.name, lastCommand.tool.name), chainCtx);
    }
    if (chainCtx.toolName() == null) {
      Command specifiedCommand = curTool.commands.stream().filter(com -> com.name.equals(chainCtx.commandName().getText())).findFirst()
          .orElseThrow(NoSuchCommandInPreviousToolException::new);
      return getValueFromChainedArgument(specifiedCommand, () -> new NoSuchOutputInSpecifiedCommandException(chainCtx.outputName().getText(), specifiedCommand.name, specifiedCommand.tool.name), chainCtx);
    }
    Iterator<Tool> tools = pipeline.tools.iterator();
    boolean hadTool = false;
    while (tools.hasNext()) {
      Tool tool = tools.next();
      if (tool.name.equals(chainCtx.toolName().getText())) {
        hadTool = true;
        Optional<Command> commandOpt = tool.commands.stream().filter(com -> com.name.equals(chainCtx.commandName().getText())).findFirst();
        if (commandOpt.isPresent()) {
          return getValueFromChainedArgument(commandOpt.get(), () -> new NoSuchOutputInSpecifiedCommandException(chainCtx.outputName().getText(), commandOpt.get().name, commandOpt.get().tool.name), chainCtx);
        }
      }
    }
    if (hadTool)
      throw new NoSuchCommandInSpecifiedToolException(chainCtx.toolName().getText(), chainCtx.commandName().getText());
    throw new NoSuchToolPreviouslyDescribedException(chainCtx.toolName().getText());
  }

  private String getValueFromChainedArgument(Command command, Supplier<RuntimeException> exceptionSupplier, PipelineParser.ChainContext chainCtx) {
    Optional<Argument> argument = command.arguments.stream().filter(arg -> arg.name.equals(chainCtx.outputName().getText())).findFirst();
    if (argument.isPresent()) {
      return argument.get().value;
    }
    return command.outputs.stream()
        .filter(opt -> opt.name.equals(chainCtx.outputName().getText())).findFirst()
        .orElseThrow(exceptionSupplier)
        .value;
  }

  private void fixArguments(Command command) {
    command.outputs.stream().forEach(output -> {
      if (output.outputType.matches("directory_dependent")) {
        command.arguments.stream().filter(arg -> arg.name.matches(output.argumentName)).findFirst().ifPresent(argu -> {output.value = argu.value + "/" + output.value;});
      }
      if (output.outputType.matches("input_dependent")) {
        command.arguments.stream().filter(arg -> arg.name.matches(output.argumentName)).findFirst().ifPresent(argu -> output.value = argu.value);
      }
      //System.out.println(output.value);
    });
    command.inputs.stream().forEach(input -> {
      if (input.inputType.matches("directory_dependent")) {
        command.arguments.stream().filter(arg -> arg.name.matches(input.argumentName)).findFirst().ifPresent(argu -> {input.value = argu.value + "/" + input.value; });
      }
      //System.out.println("ipt: " + input.value);
    });
  }

  private void fixInputsAndOuputs(Command command) {
    Iterator<Output> iterOpt = command.outputs.iterator();
    while (iterOpt.hasNext()) {
      Output opt = iterOpt.next();
      if (opt.value.isEmpty()) iterOpt.remove();
    }
    Iterator<Input> iterIpt = command.inputs.iterator();
    while (iterIpt.hasNext()) {
      Input ipt = iterIpt.next();
      if (ipt.value.isEmpty()) iterIpt.remove();
    }
  }

  private void matchInputsOutputsAndArguments(Command command) {
    command.inputs.stream().forEach(input ->
        command.arguments.stream().filter(arg -> arg.name.matches(input.argumentName) && (input.value.matches("") || (!arg.value.matches("") && !input.inputType.matches("directory_dependent")))).findFirst().ifPresent(argument -> input.value = argument.value)
    );
    command.outputs.stream().forEach(output ->
        command.arguments.stream().filter(arg -> arg.name.matches(output.argumentName) && (output.value.matches("") || (!arg.value.matches("") && !output.outputType.matches("directory_dependent")))).findFirst().ifPresent(argument -> output.value = argument.value)
    );
  }
}
