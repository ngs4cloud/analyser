package ngs4cloud.dslprocessor.exceptions.chain;

public class NoSuchCommandInSpecifiedToolException extends RuntimeException {
  public NoSuchCommandInSpecifiedToolException(String toolName, String commandName){
    super("No such command " + commandName + " issued in tool " + toolName + ".");
  }
}
