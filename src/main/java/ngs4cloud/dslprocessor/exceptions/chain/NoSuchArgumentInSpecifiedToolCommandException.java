package ngs4cloud.dslprocessor.exceptions.chain;

public class NoSuchArgumentInSpecifiedToolCommandException extends RuntimeException {
  public NoSuchArgumentInSpecifiedToolCommandException(String argumentName, String commandName, String toolName) {
    super("Argument " + argumentName + " is not described in " + toolName + ":" + commandName + " .json descriptor.");
  }
}
