package ngs4cloud.dslprocessor.exceptions.chain;

public class NoSuchOutputInSpecifiedCommandException extends RuntimeException {
  public NoSuchOutputInSpecifiedCommandException(String outputName, String commandName, String toolName){
    super("Command " + commandName + " of tool " + toolName + " is not producing output " + outputName + ".");
  }
}
