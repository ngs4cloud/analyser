package ngs4cloud.dslprocessor.exceptions.chain;

public class NoSuchOutputInPreviousCommandException extends RuntimeException {
  public NoSuchOutputInPreviousCommandException(String outputName, String commandName, String toolName) {
    super("No such output " + outputName + " in previously issued tool command " + toolName + ":" + commandName + ".");
  }
}
