package ngs4cloud.dslprocessor.exceptions.chain;

public class NoSuchToolPreviouslyDescribedException extends RuntimeException{
  public NoSuchToolPreviouslyDescribedException(String toolName){
    super("Tool " + toolName + " has not been used yet.");
  }
}
