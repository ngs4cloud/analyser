package ngs4cloud.dslprocessor.exceptions.argumentstype;

public class NoSuchArgumentTypeException extends RuntimeException {
  public NoSuchArgumentTypeException(String type) {
    super("NGS4Cloud does not support type: " + type);
  }
}
