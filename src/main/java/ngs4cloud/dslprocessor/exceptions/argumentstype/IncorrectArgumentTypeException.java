package ngs4cloud.dslprocessor.exceptions.argumentstype;

public class IncorrectArgumentTypeException extends RuntimeException {
  public IncorrectArgumentTypeException(String toolName, String commandName, String argumentName, String argumentValue, String argumentType) {
    super("Value " + argumentValue + " does not match argument " + toolName
        + ":" + commandName + ":" + argumentName + " type " + argumentType + ".");
  }
}
