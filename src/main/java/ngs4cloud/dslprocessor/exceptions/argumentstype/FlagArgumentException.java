package ngs4cloud.dslprocessor.exceptions.argumentstype;

import repository.descriptors.IArgumentDescriptor;

public class FlagArgumentException extends RuntimeException {
  public FlagArgumentException(IArgumentDescriptor argumentDescriptor, String value) {
    super(argumentDescriptor.getOriginCommand().getOriginTool().getName() + ":" + argumentDescriptor.getOriginCommand().getName() + ":"
        + argumentDescriptor.getName() + " is a flag argument so its value must be equal 'NA'. "
        + argumentDescriptor.getName() + " != " + value);
  }
}
