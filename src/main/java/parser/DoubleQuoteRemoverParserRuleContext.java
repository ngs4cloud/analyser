package parser;

import org.antlr.v4.runtime.ParserRuleContext;

public class DoubleQuoteRemoverParserRuleContext extends ParserRuleContext {

    public DoubleQuoteRemoverParserRuleContext() {
        super();
    }

    public DoubleQuoteRemoverParserRuleContext(ParserRuleContext parent, int invokingStateNumber) {
        super(parent, invokingStateNumber);
    }

    @Override
    public String getText() {
        return super.getText().replaceAll("^\"|\"$", "");
    }
}
