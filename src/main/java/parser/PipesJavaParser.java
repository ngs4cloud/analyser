/*-
 * Copyright (c) 2016, NGSPipes Team <ngspipes@gmail.com>
 * All rights reserved.
 *
 * This file is part of NGSPipes <http://ngspipes.github.io/>.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class PipesJavaParser {

    public PipelineParser.PipelineContext parsePipes(String filePath) throws IOException {
        PipelineLexer lexer = new PipelineLexer(new ANTLRInputStream(readFile(filePath)));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        PipelineParser parser = new PipelineParser(tokens);
        try{
            return parser.pipeline();
        } catch(RecognitionException e){
            throw e;
        }
    }

    /** IO **/

    private String readFileAbsolutePath(URL absolutePath, String lineSeparator) throws IOException {
        BufferedReader reader = null;
        StringBuilder fileContent = new StringBuilder();
        String line;
        try {
            reader = new BufferedReader(new InputStreamReader(absolutePath.openStream(), "UTF-8"));

            while((line = reader.readLine()) != null)
                fileContent.append(line).append(lineSeparator);
        } catch (IOException e) {
            throw new RuntimeException("Error trying to read file \n" + e.getMessage());
        } finally {
            closeBufferedReader(reader);
        }

        return fileContent.toString();
    }

    private String readFileAbsolutePath(URL absolutePath) throws IOException {
        return readFileAbsolutePath(absolutePath, "\n");
    }

    private String readFile(String filePath) throws IOException {
        return readFileAbsolutePath(getURL(filePath));
    }

    private URL getURL(String filePath) throws MalformedURLException, FileNotFoundException {
        File f = new File(filePath);

        if(f.exists())
            return f.toURI().toURL();

        URL url = this.getClass().getClassLoader().getResource(filePath);
        if(url == null) throw new FileNotFoundException("File " + filePath + " does not exist.");
        return url;
    }

    private void closeBufferedReader(BufferedReader br) throws IOException {
        try {
            if (br != null)
                br.close();
        } catch (IOException e) {
            throw e;
        }
    }

}
