package repository.entities;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Repository {

    String uri;
    String type;

    public Repository(String uri, String type) {
        this.uri = uri;
        this.type = type;
    }

}
