package repository.entities;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Input {

    public String name;
    public String description;
    public String inputType;
    public String argumentName;
    public String value;
    public Command command;

    public Input(String name, String description, String inputType, String argumentName, String value, Command command) {
        this.name = name;
        this.description = description;
        this.inputType = inputType;
        this.argumentName = argumentName;
        this.value = value;
        this.command = command;
    }
}
