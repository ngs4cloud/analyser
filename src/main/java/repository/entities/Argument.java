package repository.entities;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Argument {

    public String name;
    public String argumentType;
    public boolean isRequired;
    public String description;
    public String value;
    public Command command;

    public Argument(String name, String argumentType, boolean isRequired, String description, String value, Command command) {
        this.name = name;
        this.argumentType = argumentType;
        this.isRequired = isRequired;
        this.description = description;
        this.value = value;
        this.command = command;
    }

    public String getName() {
        return name;
    }

    public String getArgumentType() {
        return argumentType;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public String getDescription() {
        return description;
    }

    public String getValue() {
        return value;
    }
}
