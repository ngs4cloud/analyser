package repository.entities;

import java.util.List;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Command {

    public String name;
    public String command;
    public String description;
    public int priority;
    public String argumentsComposer;
    public List<Argument> arguments;
    public List<Input> inputs;
    public List<Output> outputs;
    public Tool tool;

    public Command(String name, String command, String description, int priority, String argumentsComposer, Tool tool) {
        this.name = name;
        this.command = command;
        this.description = description;
        this.priority = priority;
        this.argumentsComposer = argumentsComposer;
        this.tool = tool;
    }

}
