package repository.entities;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Output {

    public String name;
    public String description;
    public String outputType;
    public String argumentName;
    public String value;
    public Command command;

    public Output(String name, String description, String outputType, String argumentName, String value, Command command) {
        this.name = name;
        this.description = description;
        this.outputType = outputType;
        this.argumentName = argumentName;
        this.value = value;
        this.command = command;
    }
}
