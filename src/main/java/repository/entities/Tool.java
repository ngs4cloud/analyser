package repository.entities;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Tool {

    public String name;
    public String author;
    public String version;
    public String description;
    public Collection<String> documentation; //URI locations
    public int requiredMemory;
    public int recommendedCpus;
    public int recommendedDiskSpace;
    public Collection<Command> commands;
    public String dockerImage;
    public String type;
    public Pipeline pipeline;

    public Tool(String name, String author, String version, String description, Collection<String> documentation, int requiredMemory, int recommendedCpus, int recommendedDiskSpace, String dockerImage, String type, Pipeline pipeline) {
        this.name = name;
        this.author = author;
        this.version = version;
        this.description = description;
        this.documentation = documentation;
        this.requiredMemory = requiredMemory;
        this.recommendedCpus = recommendedCpus;
        this.recommendedDiskSpace = recommendedDiskSpace;
        this.commands = new LinkedList<>();
        this.dockerImage = dockerImage;
        this.type = type;
        this.pipeline = pipeline;
    }

    protected Tool() {
    }
}
