package repository.entities;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by alex_ on 27/05/2016.
 */
public class Pipeline {

    public Repository repository;
    public Collection<Tool> tools;

    public Pipeline(Repository repository) {
        this.repository = repository;
        this.tools = new LinkedList<>();
    }
}
