package repository.exceptions;

/**
 * Created by alex_ on 14/09/2016.
 */
public class NoSuchRepositoryTypeException extends RuntimeException {
  public NoSuchRepositoryTypeException(String repoType){
    super("NGS4Cloud does not support such repository type: " + repoType + ".");
  }
}
