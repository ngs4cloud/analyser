package repository;

import repository.exceptions.NoSuchRepositoryTypeException;
import repository.repository.*;

import java.util.HashMap;

/**
 * Created by alex_ on 27/05/2016.
 */
public class RepositoryFactory {

    HashMap<String, IRepository> repos = new HashMap<>();

    public RepositoryFactory(){
        repos.put("Github", new GithubRepository());
        repos.put("Local", new LocalRepository());
        repos.put("UriBased", new UriBasedRepository());
    }

    public IRepository make(String type, String location) {
        IRepository repo = repos.get(type);
        if(repo == null){
            throw new NoSuchRepositoryTypeException(type);
        }
        repo.setLocation(location);
        return repo;
    }

}
