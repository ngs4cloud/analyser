package repository.support.descriptors.json;

import repository.descriptors.InputDescriptor;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alex_ on 30/05/2016.
 */
public class JSONInputDescriptor extends InputDescriptor {
    public static final String NAME_JSON_KEY = "name";
    public static final String VALUE_JSON_KEY = "value";
    private static final String DESCRIPTION_JSON_KEY = "description";
    private static final String TYPE_JSON_KEY = "inputType";
    private static final String ARGUMENT_NAME_JSON_KEY = "argument_name";

    private final JSONObject json;

    public JSONInputDescriptor(JSONObject json) throws JSONException {
        super(	json.getString(NAME_JSON_KEY), json.getString(DESCRIPTION_JSON_KEY),
                InputDescriptor.getValue(json.getString(TYPE_JSON_KEY), json.getString(VALUE_JSON_KEY)),
                json.getString(TYPE_JSON_KEY), json.getString(ARGUMENT_NAME_JSON_KEY));

        this.json = json;
    }

    public JSONInputDescriptor(String jsonContent) throws JSONException {
        this(new JSONObject(jsonContent));
    }

    public JSONObject getJSONObject(){
        return json;
    }
}
