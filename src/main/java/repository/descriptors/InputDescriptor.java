package repository.descriptors;

import repository.utils.Utils;

/**
 * Created by alex_ on 27/05/2016.
 */
public class InputDescriptor implements IInputDescriptor {

    private ICommandDescriptor originCommand;
    private final String name;
    private final String description;
    private final String type;
    private final String value;
    private final String argumentName;

    protected static String getValue(String type, String value){
        return (value==null) ? "": value;
    }

    public InputDescriptor(String name, String description, String value, String type, String argumentName) {
        this.name = name;
        this.description = description;
        this.value = value;
        this.argumentName = argumentName;
        this.type = type;
    }

    @Override
    public ICommandDescriptor getOriginCommand() {
        return originCommand;
    }

    @Override
    public void setOriginCommand(ICommandDescriptor originCommand) {
        this.originCommand = originCommand;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getArgumentName() {
        return argumentName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o){
        if(o == null || !(o instanceof IInputDescriptor))
            return false;

        if(this == o)
            return true;

        IInputDescriptor other = (IInputDescriptor)o;

        String myName = this.getName();
        String otherName = other.getName();

        ICommandDescriptor myCommand = this.getOriginCommand();
        ICommandDescriptor otherCommand = other.getOriginCommand();

        return Utils.equals(myName, otherName) && Utils.equals(myCommand, otherCommand);
    }
}
