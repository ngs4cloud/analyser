package repository.descriptors;

/**
 * Created by alex_ on 27/05/2016.
 */
public interface IInputDescriptor {

    ICommandDescriptor getOriginCommand();

    void setOriginCommand(ICommandDescriptor originCommand);

    String getName();

    String getType();

    String getDescription();

    String getArgumentName();

    String getValue();

}

